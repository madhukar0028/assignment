class Validator{
   bool emailValidator(String value){
        String  pattern = r'^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$';
        RegExp regExp = new RegExp(pattern);
        return regExp.hasMatch(value);
  }

  bool passwordValidator(String value){
        String  pattern =  r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
        RegExp regExp = new RegExp(pattern);
        return regExp.hasMatch(value);
  }

  bool passwordLowerValidator(String value){
        String  pattern =  "[a-z]";
        RegExp regExp = new RegExp(pattern);
        return regExp.hasMatch(value);
  }

  bool passwordUpperValidator(String value){
        String  pattern =  "[A-Z]";
        RegExp regExp = new RegExp(pattern);
        return regExp.hasMatch(value);
  }

  bool passwordNumberValidator(String value){
        String  pattern =  "[0-9]";
        RegExp regExp = new RegExp(pattern);
        return regExp.hasMatch(value);
  }

  bool passwordLengthValidator(String value){
        // String  pattern =  "^[0-9]";
        // RegExp regExp = new RegExp(pattern);
        return value.length>9 ;
  }
}
import 'dart:ui';

class AssignmentColors{
  static final Color blue = Color(0xFF147eee);
  static final Color lightBlue = Color(0xFF74b1f2);
  static final Color darkBlue = Color(0xFF0849b9);
  static final Color black = Color(0xFF000000);
  static final Color white = Color(0xFFFFFFFF);
  static final Color grey = Color(0xFFF5F5F5);
  static final Color green = Color(0xFF008000);
  static final Color orange = Color(0xFFf67f15);
}
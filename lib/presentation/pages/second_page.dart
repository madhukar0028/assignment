import 'package:Assignment/core/style/color.dart';
import 'package:Assignment/core/utils/validator.dart';
import 'package:Assignment/presentation/square_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../validity_widget.dart';

class SecondPage extends StatefulWidget {
  PageController pageController;

  SecondPage({this.pageController});
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  TextEditingController _controller = TextEditingController();

  Validator validator = Validator();

  bool lowercase = false;
  bool uppercase = false;
  bool numberValidator = false;
  bool lengthValidator = false;
  bool _showPassword = true;
  bool enable = false;
  String text = '';

  _passwordValidator(String value) {
    setState(() {
      lowercase = validator.passwordLowerValidator(value);
      uppercase = validator.passwordUpperValidator(value);
      numberValidator = validator.passwordNumberValidator(value);
      lengthValidator = validator.passwordLengthValidator(value);

      _enableButton();
      if (value.length < 5) {
        text = "Very weak";
      } else {
        text = "Strong";
      }
    });
  }

  _enableButton() {
    if (lowercase && uppercase && numberValidator && lengthValidator) {
      enable = true;
    } else {
      enable = false;
    }
  }

  _checkPasswordStrength() {}

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: AssignmentColors.blue,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 170,
            ),
            Text(
              'Create Password',
              style: TextStyle(color: AssignmentColors.white, fontSize: 20),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Password will be used to login to account',
              style: TextStyle(color: AssignmentColors.white, fontSize: 15),
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              height: 60,
              padding: EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                color: AssignmentColors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: TextField(
                controller: _controller,
                onChanged: (value) {
                  _passwordValidator(value);
                },
                style: TextStyle(fontSize: 20, color: AssignmentColors.black),
                obscureText: _showPassword,
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(vertical: 20),
                    border: InputBorder.none,
                    suffixIcon: InkWell(
                      onTap: () {
                        setState(() {
                          _showPassword = !_showPassword;
                        });
                      },
                      child: Icon(
                        Icons.remove_red_eye,
                        color: AssignmentColors.blue,
                      ),
                    ),
                    hintText: 'Create Password'),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Complexity:',
                  style: TextStyle(
                    color: AssignmentColors.white,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  '$text',
                  style: TextStyle(
                    color: AssignmentColors.white,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    lowercase
                        ? ValidityWidget()
                        : Text(
                            'a',
                            style: TextStyle(
                                color: AssignmentColors.white, fontSize: 30),
                          ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Lowercase',
                      style: TextStyle(
                        color: AssignmentColors.white,
                      ),
                    )
                  ],
                ),
                Column(
                  children: [
                    uppercase
                        ? ValidityWidget()
                        : Text(
                            'A',
                            style: TextStyle(
                                color: AssignmentColors.white, fontSize: 30),
                          ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Uppercase',
                      style: TextStyle(
                        color: AssignmentColors.white,
                      ),
                    )
                  ],
                ),
                Column(
                  children: [
                    numberValidator
                        ? ValidityWidget()
                        : Text(
                            '123',
                            style: TextStyle(
                                color: AssignmentColors.white, fontSize: 30),
                          ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Number',
                      style: TextStyle(
                        color: AssignmentColors.white,
                      ),
                    )
                  ],
                ),
                Column(
                  children: [
                    lengthValidator
                        ? ValidityWidget()
                        : Text(
                            '9+',
                            style: TextStyle(
                                color: AssignmentColors.white, fontSize: 30),
                          ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      'Characters',
                      style: TextStyle(
                        color: AssignmentColors.white,
                      ),
                    )
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 80,
            ),
            SquareButton(
              enabled: enable,
              color: AssignmentColors.blue,
              onClick: (v) {
                if (v) {
                  widget.pageController.nextPage(
                      duration: kTabScrollDuration, curve: Curves.ease);
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:Assignment/core/style/color.dart';
import 'package:Assignment/core/utils/validator.dart';
import 'package:Assignment/presentation/square_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FirstPage extends StatefulWidget {
  PageController pageController;

  FirstPage({this.pageController});
  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  TextEditingController _controller = TextEditingController();

  Validator validator = Validator();
  bool enabled = false;
  int i = 0;

  _emailValidator(String value) {
    // print('value is : $value');
    // if (validator.emailValidator(value)) {
    setState(() {
      enabled = validator.emailValidator(value);
    });
    // }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('build is called ${i++}');
    return SingleChildScrollView(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipPath(
            clipper: MyClipper(),
            child: Container(height: 200, color: AssignmentColors.blue),
          ),
          // SizedBox(height: 20),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Welcome to',
                  style: TextStyle(
                      fontSize: 40,
                      color: AssignmentColors.black,
                      fontWeight: FontWeight.bold),
                ),
                Row(
                  children: [
                    Text(
                      'GIN',
                      style: TextStyle(
                          fontSize: 40,
                          color: AssignmentColors.black,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(width: 20),
                    Text(
                      'Finans',
                      style: TextStyle(
                          fontSize: 40,
                          color: AssignmentColors.blue,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Text(
                  'Welcome to The Bank of The Future. Manage and track your accounts on the go.',
                  style: TextStyle(
                    color: AssignmentColors.black,
                    fontSize: 20,
                    // fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  height: 80,
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: AssignmentColors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10),
                    ),
                  ),
                  child: Container(
                    decoration: BoxDecoration(
                        color: AssignmentColors.grey,
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: TextField(
                      controller: _controller,
                      onChanged: (value) {
                        _emailValidator(value);
                      },
                      style: TextStyle(
                          fontSize: 20, color: AssignmentColors.black),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(vertical: 20),
                          border: InputBorder.none,
                          prefixIcon: Icon(Icons.email),
                          hintText: 'Email'),
                    ),
                  ),
                ),
                SizedBox(height: 70),
                SquareButton(
                  enabled: enabled,
                  color: AssignmentColors.blue,
                  onClick: (v) {
                    if (v) {
                      setState(() {
                        widget.pageController.nextPage(
                            duration: kTabScrollDuration, curve: Curves.ease);
                      });
                    }
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0.0, size.height-20);

    var endPoint = Offset(size.width*.25, size.height-40);
    var controlPoint = Offset(size.width*0.10, size.height-50);
    path.quadraticBezierTo(
        controlPoint.dx, controlPoint.dy, endPoint.dx, endPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

import 'package:Assignment/core/style/color.dart';
import 'package:Assignment/presentation/square_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FourthPage extends StatefulWidget {
  final PageController pageController;

  const FourthPage({Key key, this.pageController}) : super(key: key);
  @override
  _FourthPageState createState() => _FourthPageState();
}

class _FourthPageState extends State<FourthPage> {
  DateTime pickedDate;
  TimeOfDay time;

  _pickDate(BuildContext context) async {
    DateTime date = await showDatePicker(
      context: context,
      firstDate: DateTime(DateTime.now().month - 1),
      lastDate: DateTime(DateTime.now().year + 5),
      initialDate: DateTime.now(),
    );
    if (date != null)
      setState(() {
        pickedDate = date;
      });
  }

  _pickTime() async {
    TimeOfDay t =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    if (t != null)
      setState(() {
        time = t;
      });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: AssignmentColors.blue,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 140,
          ),
          InkWell(
            onTap: () {
              _pickDate(context);
            },
            child: Container(
              height: 70,
              width: 70,
              decoration: BoxDecoration(
                color: AssignmentColors.white,
                shape: BoxShape.circle,
                border: Border.all(
                  width: 15,
                  color: AssignmentColors.lightBlue,
                ),
              ),
              child: Center(
                child: Icon(
                  Icons.calendar_today,
                  size: 25,
                  color: AssignmentColors.blue,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Text(
            'Schedule Video Call',
            style: TextStyle(color: AssignmentColors.white, fontSize: 25),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Chose the date and time you preferred, we will send a link via email to make a video call on the scheduled date and time',
            style: TextStyle(color: AssignmentColors.white, fontSize: 18),
          ),
          SizedBox(
            height: 50,
          ),
          InkWell(
            onTap: () {
              _pickDate(context);
            },
            child: Container(
              height: 70,
              width: 370,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: AssignmentColors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Date'),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    pickedDate != null
                        ? '${pickedDate.day}-${pickedDate.month}-${pickedDate.year}'
                        : "-Chose Date-",
                    style: TextStyle(fontSize: 20),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {
              _pickTime();
            },
            child: Container(
              height: 70,
              width: 370,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: AssignmentColors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Time'),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    time != null
                        ? '${time.hour}:${time.minute}'
                        : '-Chose Time-',
                    style: TextStyle(fontSize: 20),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 60,
          ),
          SquareButton(
            enabled: pickedDate != null && time != null,
            color: AssignmentColors.lightBlue,
            onClick: (v) {
              if (v) {
                widget.pageController
                    .nextPage(duration: kTabScrollDuration, curve: Curves.ease);
              }
            },
          )
        ],
      ),
    );
  }
}

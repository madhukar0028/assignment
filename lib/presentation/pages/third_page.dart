import 'package:Assignment/core/style/color.dart';
import 'package:Assignment/presentation/drop_down_widget.dart';
import 'package:Assignment/presentation/square_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThirdPage extends StatefulWidget {

  final PageController pageController;

  const ThirdPage({Key key, this.pageController}) : super(key: key);
  @override
  _ThirdPageState createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> {
  List<String> goalItems = ['Goal1', 'Goal2', 'Goal3', 'Goal4'];
  List<String> incomeItems = ['10000', '20000', '30000', '40000'];
  List<String> expenseItems = ['Expense1', 'Expense2', 'Expense3', 'Expense4'];
  bool goalText = false;
  bool incomeText = false;
  bool expenseText = false;
  bool enabled = false;

  _enableButton() {
    if (goalText && incomeText && expenseText) {
      enabled = true;
    } else {
      enabled = false;
    }
    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _enableButton();
    print('button is $enabled');
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: AssignmentColors.blue,
        padding: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 170,
            ),
            Text(
              'Personal Information',
              style: TextStyle(color: AssignmentColors.white, fontSize: 22),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Please fill in the information below and your goal for digital saving',
              style: TextStyle(color: AssignmentColors.white, fontSize: 18),
            ),
            SizedBox(
              height: 60,
            ),
            Container(
              height: 60,
              // width: 300,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: AssignmentColors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text('Goal for activation'),
                  DropdownWidget(
                    item: goalItems,
                    selectedItem: (s) {
                      goalText = s;
                      _enableButton();
                    },
                  )
                ],
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              height: 60,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: AssignmentColors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Monthly income'),
                  DropdownWidget(
                    item: incomeItems,
                    selectedItem: (s) {
                      incomeText = s;
                      _enableButton();
                    },
                  )
                ],
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              height: 60,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: AssignmentColors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Monthly expense'),
                  DropdownWidget(
                    item: expenseItems,
                    selectedItem: (s) {
                      expenseText = s;
                      _enableButton();
                    },
                  )
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            SquareButton(
              enabled: enabled,
              color: AssignmentColors.lightBlue,
              onClick: (v) {
                if (v) {
                  widget.pageController.nextPage(
                      duration: kTabScrollDuration, curve: Curves.ease);
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:Assignment/core/style/color.dart';
import 'package:flutter/cupertino.dart';

class FifthPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: AssignmentColors.blue,
      child: Center(
        child: Text(
          'Thank you',
          style: TextStyle(fontSize: 30, color: AssignmentColors.white),
        ),
      ),
    );
  }
}

import 'package:Assignment/core/style/color.dart';
import 'package:Assignment/presentation/page_view_indicator.dart';
import 'package:Assignment/presentation/pages/fifth_page.dart';
import 'package:Assignment/presentation/pages/first_page.dart';
import 'package:Assignment/presentation/pages/fourth_page.dart';
import 'package:Assignment/presentation/pages/second_page.dart';
import 'package:Assignment/presentation/pages/third_page.dart';
import 'package:flutter/material.dart';

class FirstScreen extends StatefulWidget {
  @override
  _FirstScreenState createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  PageController _controller = PageController(initialPage: 0);
  int _currentPage = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      color: AssignmentColors.grey,
      child: Stack(
        children: [
          PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: _controller,
            onPageChanged: (int i) {
              setState(() {
                _currentPage = i;
              });
            },
            children: [
              FirstPage(pageController: _controller,),
              SecondPage(pageController: _controller,),
              ThirdPage(pageController: _controller,),
              FourthPage(pageController: _controller,),
              FifthPage(),
            ],
          ),
          Positioned(
            top: 30,
            left: 20,
            child: PageViewIndicator(
              currentPage: _currentPage,
              onTap: (b) {
                if (b) {
                  setState(() {
                    _currentPage = 0;
                    _controller.jumpTo(0);
                  });
                }
              },
            ),
          )
        ],
      ),
    );
  }
}

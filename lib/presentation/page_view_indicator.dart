import 'package:Assignment/core/style/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PageViewIndicator extends StatelessWidget {
  int currentPage;
  Function(bool) onTap;

  PageViewIndicator({Key key, this.currentPage, this.onTap}) : super(key: key);

  List<Widget> _createPageIndicator(int currentPage) {
    List<Widget> indicator = [];
    print('current page is: $currentPage');
    indicator.add(SizedBox(width: 20));
    for (int i = 0; i < 4; i++) {
      indicator.add(
        Container(
          height: 50,
          width: 50,
          decoration: BoxDecoration(
              color: i < currentPage
                  ? AssignmentColors.green
                  : AssignmentColors.white,
              shape: BoxShape.circle,
              border: Border.all(color: AssignmentColors.black, width: 1)),
          child: Center(
            child: Text(
              '${i + 1}',
              style: TextStyle(fontSize: 20, color: AssignmentColors.black),
            ),
          ),
        ),
      );
      if (i < 3) {
        indicator.add(
          Container(height: 2, width: 40, color: AssignmentColors.black),
        );
      }
    }
    return indicator;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        currentPage != 0
            ? Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: (){
                      onTap(true);
                      currentPage = 0;
                    },
                    child: Icon(
                      Icons.arrow_back,
                      color: AssignmentColors.white,
                    ),
                  ),
                  SizedBox(width: 10),
                  Text(
                    'Create Acount',
                    style:
                        TextStyle(color: AssignmentColors.white, fontSize: 20),
                  )
                ],
              )
            : Container(height: 23),
        SizedBox(height: 20),
        Row(children: _createPageIndicator(currentPage)),
      ],
    );
  }
}

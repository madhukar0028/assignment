import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropdownWidget extends StatefulWidget {
  final List<String> item;
  Function(bool) selectedItem;

  DropdownWidget({Key key, this.item, this.selectedItem}) : super(key: key);

  @override
  _DropdownWidgetState createState() => _DropdownWidgetState();
}

class _DropdownWidgetState extends State<DropdownWidget> {
  String selectedItem;
  List<String> goalItems = ['Goal1', 'Goal2', 'Goal3', 'Goal4'];

  @override
  Widget build(BuildContext context) {
    print('items are ${widget.item}');
    return DropdownButtonHideUnderline(
      child: DropdownButton<String>(
        hint: Text("-Chose Option-"),
        value: selectedItem,
        isExpanded: true,
        isDense: true,
        items: widget.item.map((String value) {
          return new DropdownMenuItem<String>(
            value: value,
            child: new Text(value),
          );
        }).toList(),
        onChanged: (v) {
          setState(() {
            selectedItem = v;
            widget.selectedItem(true);
          });
        },
      ),
    );
  }
}

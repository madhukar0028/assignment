import 'package:Assignment/core/style/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SquareButton extends StatelessWidget {
  final bool enabled;
  final Color color;
  final Function(bool) onClick;

  const SquareButton({Key key, this.enabled, this.color, this.onClick})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    print('$enabled');
    return InkWell(
      onTap: () {
        if (enabled) {
          onClick(true);
        }
      },
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          color: enabled ? AssignmentColors.darkBlue : AssignmentColors.lightBlue,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Center(
          child: Text(
            'Next',
            style: TextStyle(
                color: enabled
                    ? AssignmentColors.white
                    : AssignmentColors.white.withOpacity(.6),
                fontSize: 20),
          ),
        ),
      ),
    );
  }
}

import 'package:Assignment/core/style/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ValidityWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      width: 30,
      decoration:
          BoxDecoration(color: AssignmentColors.green, shape: BoxShape.circle),
      child: Center(
        child: Icon(
          Icons.done,
          size: 20,
          color: AssignmentColors.white,
        ),
      ),
    );
  }
}
